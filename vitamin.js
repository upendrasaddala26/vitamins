const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


// problem1

function available(){
    const allitems=items.filter(function(element){
        if (element.available===true){
            return element;
        }
    })
    return allitems;

}
// console.log(available());


// / problem2

function Vitamins(){
    const allitems =items.filter(function(element){
        if (element.contains == "Vitamin C"){
            return element;
        }
        
    })
    return allitems;
}
// console.log(Vitamins());

// problem3

let vitaminscontainsA = items.filter(values => values.contains.includes("Vitamin A"))
// console.log(vitaminscontainsA);

// problem4

let grouping = items.reduce((accumulator,currentvalue)=>{
    currentvalue.contains.split(", ").map(vitamincontain => {
        if (accumulator[vitamincontain]){
            accumulator[vitamincontain]=accumulator[vitamincontain]+", "+ currentvalue.name;

        }else{
            accumulator[vitamincontain]=currentvalue.name;
        }
        return vitamincontain;
    })
    return accumulator;
},{});
// console.log(grouping);

// problem5
let sortitems = items.sort((firstvalue,secondvalue)=>{
    let item1 = firstvalue.contains.split(",").length;
    let item2 = secondvalue.contains.split(",").length;
    if (item1>item2){
        return -1;
    }else{
        return 1;
    }
});
console.log(sortitems);